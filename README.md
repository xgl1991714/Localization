# LWOTC Beta Localization

#### 介绍
LWOTC beta版本的中文翻译

---------------初始化------------------------------

#克隆仓库代码到本地

git clone https://gitee.com/xgl1991714/Localization.git

#进入文件夹

cd Localization

#配置邮箱地址（记得邮箱改成自己注册时用到的）

git config user.email 691976751@qq.com

#配置用户名（记得邮箱改成自己注册时用到的）

git config user.name 691976751@qq.com

#查看所有分支（只是看看）

git branch -a


-----------------翻译阶段--------------------------

#拉取仓库里的最新代码（其他的人的改动）

git pull

#查看本地当前分支的状态（是否有需要提交的内容）

git status

#将全部改动文件记录在本地缓存

git add *

#将记录在本地缓存文件改动提交到本地分支，并附带改动描述

git commit -m "更新翻译"

#将本地分支的改动提交到远程分支

git push

